import 'package:flutter/material.dart';
import 'package:gastos/pages/home_page.dart';
import 'package:gastos/pages/login_page.dart';
import 'package:provider/provider.dart';
import 'components/login_state.dart';
import 'components/page_detalhes.dart';


void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginState>(
      builder: (BuildContext context) => LoginState(),
      child: MaterialApp(
        title: 'Controle Gastos',
        theme: ThemeData(
          primaryColor: Color(0xFF646ecb),
        ),
        onGenerateRoute: (settings) {
          if (settings.name == '/details') {
            DetailsParams params = settings.arguments;
            return MaterialPageRoute(
              builder: (BuildContext context) {
                return DetailsPage(
                  params: params,
                );
              }
            );
          }
        },
        routes: {
          '/': (BuildContext context) {
            var state = Provider.of<LoginState>(context);
            if (state.isLoggedIn()) {
              return HomePage();
            } 
            else {
              return LoginPage();
            }
          },
        },
      ),
    );
  }
}
