import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gastos/components/add_page.dart';
import 'package:gastos/components/add_transicao.dart';
import 'package:gastos/components/login_state.dart';
import 'package:gastos/components/mes_widget.dart';
import 'package:provider/provider.dart';
import 'package:rect_getter/rect_getter.dart';

import '../contants.dart';

class HomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  var globalKey = RectGetter.createGlobalKey();
  Rect buttonRect;
  int currentPage = DateTime.now().month - 1;
  PageController _controller;
  Stream<QuerySnapshot> _query;
  GraphType currentType = GraphType.LINES;

  @override
  void initState() {
    super.initState();

    _controller = PageController(
      initialPage: currentPage,
      viewportFraction: 0.4,
    );
  }

  Widget _bottomAction(IconData icon, Function callback) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Icon(icon),
      ),
      onTap: callback,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginState>(
        builder: (BuildContext context, LoginState state, Widget child) {
      var user = Provider.of<LoginState>(context).currentUser();
      _query = Firestore.instance
          .collection('users')
          .document(user.uid)
          .collection('despesas')
          .where("mes", isEqualTo: currentPage + 1)
          .snapshots();

      return Scaffold(
        bottomNavigationBar: BottomAppBar(
          notchMargin: 8.0,
          shape: CircularNotchedRectangle(),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[   
              _bottomAction(FontAwesomeIcons.chartLine, () {
                setState(() {
                  currentType = GraphType.LINES;
                });
              }),
              _bottomAction(FontAwesomeIcons.chartPie, () {
                setState(() {
                  currentType = GraphType.PIE;
                });
              }),
              // SizedBox(width: 48.0),
              // _bottomAction(FontAwesomeIcons.wallet, () {}),
              _bottomAction(FontAwesomeIcons.signOutAlt, () {
                Provider.of<LoginState>(context).logout();
              }),
              SizedBox(),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        floatingActionButton: RectGetter(
          key: globalKey,
          child: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              buttonRect = RectGetter.getRectFromKey(globalKey);

              print(buttonRect);

              var page = AddPageTransicao(
                background: widget,
                pagina: AddPage(
                  buttonRect: buttonRect,
                ),
              );

              Navigator.of(context).push(page);
            },
          ),
        ),
        body: _body(),
      );
    });
  }

  Widget _body() {
    return SafeArea(
      child: Column(
        children: <Widget>[
          _selector(),
          StreamBuilder<QuerySnapshot>(
            stream: _query,
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> data) {
              if (data.hasData) {
                return MonthWidget(
                  days: daysInMonth(currentPage + 1),
                  documents: data.data.documents,
                  graphType: currentType,
                  month: currentPage,
                );
              }

              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _pageItem(String name, int position) {
    var _alignment;
    final selected = TextStyle(
      fontSize: 20.0,
      fontWeight: FontWeight.bold,
      color: Colors.blueGrey,
    );
    final unselected = TextStyle(
      fontSize: 20.0,
      fontWeight: FontWeight.normal,
      color: Colors.blueGrey.withOpacity(0.4),
    );

    if (position == currentPage) {
      _alignment = Alignment.center;
    } else if (position > currentPage) {
      _alignment = Alignment.centerRight;
    } else {
      _alignment = Alignment.centerLeft;
    }

    return Align(
      alignment: _alignment,
      child: Text(
        name,
        style: position == currentPage ? selected : unselected,
      ),
    );
  }

  Widget _selector() {
    return SizedBox.fromSize(
      size: Size.fromHeight(70.0),
      child: PageView(
        onPageChanged: (newPage) {
          setState(() {
            var user = Provider.of<LoginState>(context).currentUser();
            currentPage = newPage;
            _query = Firestore.instance
                .collection('users')
                .document(user.uid)
                .collection('despesas')
                .where("mes", isEqualTo: currentPage + 1)
                .snapshots();
          });
        },
        controller: _controller,
        children: <Widget>[
          _pageItem("Janeiro", 0),
          _pageItem("Fevereiro", 1),
          _pageItem("Março", 2),
          _pageItem("Abril", 3),
          _pageItem("Maio", 4),
          _pageItem("Junho", 5),
          _pageItem("Julho", 6),
          _pageItem("Agosto", 7),
          _pageItem("Setembro", 8),
          _pageItem("Outubro", 9),
          _pageItem("Novembro", 10),
          _pageItem("Dezembro", 11),
        ],
      ),
    );
  }
}