import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gastos/components/login_state.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Consumer<LoginState>(
          builder: (BuildContext context, LoginState value, Widget child) {
            if (value.isLoading()) {
              return CircularProgressIndicator();
            } else {
              return child;
            }
          },
          child: FloatingActionButton(
            child: Icon(FontAwesomeIcons.facebookF),
            backgroundColor: Color(0xff3b5998),
            onPressed: () {
              Provider.of<LoginState>(context).login();
            },
          ),
        ),
      ),
    );
  }
}