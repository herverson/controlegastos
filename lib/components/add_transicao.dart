import 'package:flutter/material.dart';

class AddPageTransicao extends PageRouteBuilder {
  final Widget pagina;
  final Widget background;

  AddPageTransicao({this.pagina, this.background}) : super(
    transitionDuration: Duration(microseconds: 1),
    pageBuilder: (context, animacao1, animacao2) => pagina,
    transitionsBuilder: (context, animation1, animation, child) =>
      Stack(
        children: <Widget>[
          background,
          child
        ],
      )
    );
  
}