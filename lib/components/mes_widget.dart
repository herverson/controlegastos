import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gastos/components/page_detalhes.dart';
import 'package:intl/intl.dart';
import 'grafico.dart';

enum GraphType {
  LINES,
  PIE,
}

class MonthWidget extends StatefulWidget {
  final List<DocumentSnapshot> documents;
  final double total;
  final List<double> perDay;
  final Map<String, double> categories;
  final Map<String, IconData> categoriesIcons;
  final GraphType graphType;
  final int month;

  MonthWidget({Key key, this.month, this.graphType, this.documents, days})
      :
        total = documents.map((doc) => doc['valor'])
            .fold(0.0, (a, b) => a + b),
        perDay = List.generate(days, (int index) {
          return documents.where((doc) => doc['dia'] == (index + 1))
              .map((doc) => doc['valor'])
              .fold(0.0, (a, b) => a + b);
        }),
        categories = documents.fold({}, (Map<String, double> map, document) {
          if (!map.containsKey(document['categoria'])) {
            map[document['categoria']] = 0.0;
          }

          map[document['categoria']] += document['valor'];
          return map;
        }),
        categoriesIcons = documents.fold({}, (Map<String, IconData> map, document) {
          switch (document['categoria']) {
            case 'Compras':
              map[document['categoria']] = Icons.shopping_cart;
              break;
            case 'Álcool':
              map[document['categoria']] = FontAwesomeIcons.beer;
              break;
            case 'Fast food':
              map[document['categoria']] = FontAwesomeIcons.hamburger;
              break;
            case 'Outra':
              map[document['categoria']] = FontAwesomeIcons.moneyBill;
              break;
            case 'Conta':            
              map[document['categoria']] = FontAwesomeIcons.wallet;
              break;
            default:
          }
          return map;
        }),
        super(key: key);

  @override
  _MonthWidgetState createState() => _MonthWidgetState();
}

class _MonthWidgetState extends State<MonthWidget> {
  @override
  Widget build(BuildContext context) {
    print(widget.categories);
    return Expanded(
      child: Column(
        children: <Widget>[
          _expenses(),
          _graph(),
          Container(
            color: Colors.blueAccent.withOpacity(0.15),
            height: 24.0,
          ),
          _list(),
        ],
      ),
    );
  }

  Widget _expenses() {
    return Column(
      children: <Widget>[
        Text(
          "R\$ ${NumberFormat('0.00', 'pt_BR').format(widget.total)}",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 40.0,
          ),
        ),
        Text("Total de despesas",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
            color: Colors.blueGrey,
          ),
        ),
      ],
    );
  }

  Widget _graph() {
    if (widget.graphType == GraphType.LINES) {
      return Container(
        height: 150.0,
        child: LinesGraphWidget(
          data: widget.perDay,
        ),
      );
    } 
    else {
      var perCategory = widget.categories.keys.map((name) => widget.categories[name] / widget.total).toList();
      return Container(
        height: 150.0,
        child: PieGraphWidget(
          data: perCategory,
        ),
      );
    }
  }

  Widget _item(IconData icon, String name, int percent, double value) {
    return ListTile(
      onTap: () {
        Navigator.of(context).pushNamed("/details",
            arguments: DetailsParams(name, widget.month));
      },
      leading: Icon(icon, size: 32.0,),
      title: Text(name,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.0
        ),
      ),
      subtitle: Text("$percent% das despesas",
        style: TextStyle(
          fontSize: 16.0,
          color: Colors.blueGrey,
        ),
      ),
      trailing: Container(
        decoration: BoxDecoration(
          color: Colors.blueAccent.withOpacity(0.2),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "R\$ ${NumberFormat('0.00', 'pt_BR').format(value)}",
            style: TextStyle(
              color: Colors.blueAccent,
              fontWeight: FontWeight.w500,
              fontSize: 16.0,
            ),
          ),
        ),
      ),
    );
  }

  Widget _list() {
    return Expanded(
      child: ListView.separated(
        itemCount: widget.categories.keys.length,
        itemBuilder: (BuildContext context, int index) {
          var key = widget.categories.keys.elementAt(index);
          var data = widget.categories[key];
          var icon  = widget.categoriesIcons[key];
          
          return _item(icon, key, 100 * data ~/ widget.total, data);
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            color: Colors.blueAccent.withOpacity(0.15),
            height: 8.0,
          );
        },
      ),
    );
  }
}